## shed -- baSH EDitor, a text editor written in bash

Yup.

### Usage

    shed <file> <starting line>

`<file>` is required. `<starting line>` is optional

After that, just use 'h' to learn how to use this.

You can also try it on the included file "test"

### Installation

    sudo ./install    # to install
    sudo ./install -u # to uninstall

### Closing notes

I don't know why I wrote this, but it's pretty neat. It even has readline and a few other frills that ed lacks.

In fact, I wrote this README using only shed